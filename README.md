# README #

This README would normally document whatever steps are necessary to get your application up and running.

I used composer to autoload classes and to maintain easy to read structure. I also installed PHPUnit for testing purposes.
### What is this repository for? ###

* Finite State Machine for Modulo 3
* Initial Assessment

### How do I get set up and run? ###

* git clone https://saqibbilal@bitbucket.org/saqibbilal/finite_state_machine.git
* cd finite_state_machine
* composer install 
* Set $inputString value in policyReporter.php file
* go to your server path in a browser e.g. http://localhost/finite_state_machine/
* To run the tests, run this command in root directory using terminal/cmd: vendor/bin/phpunit tests/ModuloTests.php

