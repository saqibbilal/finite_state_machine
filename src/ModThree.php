<?php

namespace modulo;
use Exception;

/**
 * This class calculates the remainder of modulo 3
 */
class ModThree{

    /** @var string $currentState is set to initial state S0 */
    private $currentState = 'S0';
    // available states for modulo 3
    /** @var array $stateOutputs contains possible states for modulo 3 */
    private $stateOutputs = array(
        "S0",
        "S1",
        "S2",
    );

    /**
     * Constructor does not do much in our scenario.
     */
    public function __construct()
    {
        // comment out below lines for better readability of test cases
        echo 'Welcome to Finite State Machine for Module 3<br>';
        echo '--------------------------------------------<br>';
    }


    /**
     * This is the main function that gets called with one parameter.
     *
     * @param string $input a binary string of 0s and 1s.
     *
     * @return int
     */
    public function modThree(string $input = "") : int
    {
//      un-comment this block to test testExceptions() in ModuloTests.php
//        if(empty($input)){
//            throw new \Exception("Please provide a binary input");
//        }

        try{
            if(empty($input)){
                throw new \Exception("Please provide a binary input");
            }
            $bitsArray = str_split($input);
            foreach($bitsArray as $bit){
                if(intval($bit) > 1)
                    throw new \Exception("invalid input > Please provide a binary string of 0s and 1s");
                $this->setState($bit);
            }
        }catch (Exception $e) {
            echo 'Caught exception: <br>',  $e->getMessage(), "\n";
            exit();
        }

        return $this->getFinal_state();
    }


    /**
     * This function sets currentState property.
     *
     * @param string $inputBit a single bit; 0 or 1.
     *
     * @return void
     */
    private function setState(string $inputBit){
        if(trim($inputBit) === "0" or trim($inputBit) === "1"){
            switch ($this->currentState) {
                case "S0":
                    if($inputBit ==="0"){
                        $this->currentState = $this->stateOutputs[0];
                    }else{
                        $this->currentState = $this->stateOutputs[1];
                    }
                    break;

                case "S1":
                    if($inputBit ==="0"){
                        $this->currentState = $this->stateOutputs[2];
                    }else{
                        $this->currentState = $this->stateOutputs[0];
                    }
                    break;
                default:
                    if($inputBit ==="0"){
                        $this->currentState = $this->stateOutputs[1];
                    }else{
                        $this->currentState = $this->stateOutputs[2];
                    }
            }
        }else{
            echo " Something went wrong!! ";
        }
    }


    /**
     * This function gets the final state of FSM and makes sure it is an integer before returning.
     *
     * @return int
     */
    public function getFinal_state() : int{
        return ((int)trim($this->currentState,"S"));
    }


    /**
     * This function gets the initial state of currentState.
     *
     * This function is for UnitTests
     *
     * @return string
     */
    public function getInitialState(){
        return $this->currentState;
    }


    /**
     * This function gets the stateOutputs property of FSM.
     *
     * This function is for UnitTests
     *
     * @return array
     */
    public function getStateOutputs(){
        return $this->stateOutputs;
    }

    /**
     * This function resets the initial state to S0. caused me enough trouble during testing
     *
     * This function is for UnitTests.
     *
     * @return void
     */
    public function resetInitialState(){
        $this->currentState = "S0";
    }
}