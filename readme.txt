First off, Thank you for creating this text David, took me a while to omplete the 5 questions so skipping the bonus
question for now.

Sorry for the delay, I was not expecting it to be this lengthy. I started on sunday at around 3:00 pm and finished
at around 3:30 am (Monday). also took some breaks for about 3 to 4 hours.
----------
I used composer and PHP 7.3.12.

I am auto-loading classes from src directory using composer with namespace: D_mob and using psr-4 standard

Please follow these steps to setup:
    paste the donkey_mob directory in a php environment and composer should be included at the path 
    or
    in xampp/htdocs directory if you use xampp or www directory if you use wamp

run the following command to generate "vendor" directory:
    composer install


Entry point is index.php and then moving into donkeyMob.php
In donkeyMob.php file, I am only making objects of classes and printing the results for questions

First 3 questions are done in donkeyMob.php

Question 4 sql file is placed in "database schema for Q4" directory at root. I created the schema in phpmyadmin
and exported the file.

Question 5 html file is placed in "views" directory at root. jQuery would have been easier to use but I tried in js in
order to stick to the question statement. used google for help but I believe it's a part of the job.

