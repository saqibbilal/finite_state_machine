<?php
//using class from modulo namespace. all classes should be placed in src directory
use modulo\ModThree;

/** $inputString is the input binary string */
$inputString = "1101";

// making an object of ModThree class
$output = (new ModThree())->modThree($inputString);

print_r($output);