<?php
namespace modulo;

use PHPUnit\Framework\TestCase;

class ModuloTests extends TestCase{

    /**
     * This test checks the initial state of the object.
     * It should be S0
     */
    function testInitialState(){
        $modulo3 = new ModThree();
        $this->assertEquals("S0",$modulo3->getInitialState());
    }

    /**
     * This test checks the initial state of the object.
     * It should be S0
     */
    function testStateOutputsPropertyIsAnArray(){
        $modulo3 = new ModThree();
        $this->assertIsArray($modulo3->getStateOutputs());
    }

    function testOutput(){
        $modulo3 = new ModThree();
        $this->assertSame(2,$modulo3->modThree("1110"));

        $modulo3->resetInitialState();
        $this->assertSame(1,$modulo3->modThree("1101"));

        $modulo3->resetInitialState();
        $this->assertSame(0,$modulo3->modThree("1111"));
    }



// this exception is not caught because it has been hanfled in code with try/catch.
//    /**
//     * This test checks if no string is passed to modThree().
//     * This method requires a string parameter
//     */
//    function testExceptions(){
//        $modulo3 = new ModThree();
//        $this->expectException("Exception");
//        $this->expectExceptionMessage("Please provide a binary input");
//        $modulo3->modThree();
//    }

}